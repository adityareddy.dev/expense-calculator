# Expense Calculator

Expense calculator is a nodejs and react project that lets you input 3 numeric values and 3 strings representing 3 students that need to exchange money and even out to the average rounded off to two decimal points.

For simplicity sake the program assumes three students, two of whom pay way less than the other. Use inpiuts accordingly

## Build

It was built with node and react. The react part is in the folder expense-calculator-ui and the node server is in the folder expense-calculator-server. The code is hosted on gitlab. It has a Docker File that builds the react part first and then builds the server part. The express nodejs server serves the static files of the build at the root /. The server has three routes get /, get /transactions, post /transactions. The docker image is then deployed to GKE(Google Kubernetes Engine) cluster with the help of kubernetes (deployment.yaml in the root).

(http://localhost:8080 if running locally

## Installation

To install run the script 'install-all' in the root folder of the project. This will install dependencies for the root, server and the UI

```bash
npm run install-all
```

## Running it locally

All you have to do is run the start script, it builds the react part first and then builds the server part. The express nodejs server serves the static files of the build at the root /

```bash
npm run start
```

and then visit:

```bash
http://localhost:8080/
```

## Deployment

To deploy any changes all you have to do is commit to the 'main' branch and the gitlab runner will pick it up, build the docker image and push to google cloud. You should be able to see changes.

You can see the pipelines running here

```bash
https://gitlab.com/adityareddy.dev/expense-calculator/-/pipelines
```

You can view the app in the kube cluster at:

```bash
http://34.160.150.214/
```

I chose google cloud cause I had never worked with it and thought it was a good opportunity to experiment, plus they had like 300$ in free credits!

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
