const express = require("express");
const path = require("path");
const fs = require("fs");
const app = express();

//express can parse json
app.use(express.json());

//this gives express the path to our static build folder (ui)
app.use(express.static(path.join(__dirname, "../expense-calculator-ui/build")));

//this serves the front end of the app
app.get("/", (req, res) => {
  res.sendFile(
    path.join(__dirname, "../expense-calculator-ui/build/index.html")
  );
});

//returns the transactions from transactions.json file
app.get("/transactions", (req, res) => {
  res.status(200);
  res.json({
    success: true,
    transactions: JSON.parse(fs.readFileSync("./transactions.json"))
      .transactions,
  });
});

//posts the incoming transactions to the transactions.json file, holds only 6 max
app.post("/transactions", (req, res) => {
  const data = JSON.parse(fs.readFileSync("./transactions.json"));
  req.body.forEach((element) => {
    if (data.transactions.length >= 6) data.transactions.shift();
    data.transactions.push(element);
  });
  fs.writeFileSync("./transactions.json", JSON.stringify(data));
  res.status(200);
  res.json({
    success: true,
    msg: "Data successfully written to file",
  });
});

//run the server on 8080
app.listen(8080, () => {
  console.log("Server running on port 8080");
});
