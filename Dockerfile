FROM node:alpine AS ui-build
WORKDIR /usr/src/app
COPY expense-calculator-ui/ ./expense-calculator-ui/
RUN cd expense-calculator-ui && npm install && npm run build

FROM node:alpine AS server-build
WORKDIR /root/
COPY --from=ui-build /usr/src/app/expense-calculator-ui/build ./expense-calculator-ui/build
COPY expense-calculator-server/package*.json ./expense-calculator-server/
COPY expense-calculator-server/index.js ./expense-calculator-server/
COPY expense-calculator-server/transactions.json ./
RUN cd expense-calculator-server && npm install

EXPOSE 8080

CMD ["node", "./expense-calculator-server/index.js"]