const initialState = [
  { id: 0, name: "", amount: 0 },
  { id: 1, name: "", amount: 0 },
  { id: 2, name: "", amount: 0 },
];

const studentAndAmountsReducer = (state = initialState, action) => {
  let newState = [];
  switch (action.type) {
    case "studentNameChange":
      newState = state.map((student) => {
        if (student.id === action.payload.id) {
          return { ...student, name: action.payload.name };
        }
        return student;
      });
      return newState;
    case "studentAmountChange":
      newState = state.map((student) => {
        if (student.id === action.payload.id) {
          return { ...student, amount: action.payload.amount };
        }
        return student;
      });
      return newState;
    case "resetState":
      return initialState;
    default:
      return state;
  }
};

export default studentAndAmountsReducer;
