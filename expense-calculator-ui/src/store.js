//Setting up our redux store
import { configureStore } from "@reduxjs/toolkit";
import studentAndAmountsReducer from "./reducers/student-amount-reducer";

export default configureStore({
  reducer: studentAndAmountsReducer,
});
