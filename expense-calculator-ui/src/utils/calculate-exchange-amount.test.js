const calculateExchange = require("./calculate-exchange-amount");

const testStudentsObject = [
  { name: "Test0", amount: 20 },
  { name: "Test1", amount: 10 },
  { name: "Test2", amount: 50 },
];
const testReturnObject = [
  { name: "Test1", amount: 16.67, toGive: "Test2" },
  { name: "Test0", amount: 6.67, toGive: "Test2" },
];

test("gives exchange amount of each student", () => {
  expect(calculateExchange(testStudentsObject)).toStrictEqual(testReturnObject);
});
