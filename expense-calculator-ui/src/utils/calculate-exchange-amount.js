//Function assumes it receives an array of objects
//where the amount in one object is larger than the average and the other two amounts are less than the average
//to calculate how much money has to be exchanged.
const calculateExchange = (studentsAndAmounts) => {
  //Calculate total spend by all students
  const totalAmount = studentsAndAmounts.reduce((total, student) => {
    return total + student.amount;
  }, 0);

  //Sort in ascending order
  const studentsAndAmountsSorted = studentsAndAmounts.sort((a, b) =>
    a.amount > b.amount ? 1 : -1
  );

  //Calculate average and then difference from average,
  //round off to two decimal points and create object
  return [
    {
      name: studentsAndAmountsSorted[0].name,
      amount:
        Math.round(
          (totalAmount / 3 - studentsAndAmountsSorted[0].amount) * 100
        ) / 100,
      toGive: studentsAndAmountsSorted[2].name,
    },
    {
      name: studentsAndAmounts[1].name,
      amount:
        Math.round(
          (totalAmount / 3 - studentsAndAmountsSorted[1].amount) * 100
        ) / 100,
      toGive: studentsAndAmountsSorted[2].name,
    },
  ];
};

module.exports = calculateExchange;
