import React from "react";
import { TextField, FormControl } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import "./index.css";

const Student = (props) => {
  //get the data from the redux store using the student id
  const student = useSelector((state) => state[props.id]);
  const dispatch = useDispatch();

  //dispatch a name change action
  const handleStudentNameChange = (e) => {
    if (e) e.preventDefault();
    dispatch({
      type: "studentNameChange",
      payload: { id: props.id, name: e.target.value },
    });
  };

  //dispatch an amount change action
  const handleStudentExpensesChange = (e) => {
    if (e) e.preventDefault();
    let amount = 0;
    if (!isNaN(parseFloat(e.target.value))) {
      amount = parseFloat(e.target.value);
    }
    dispatch({
      type: "studentAmountChange",
      payload: { id: props.id, amount: parseFloat(amount) },
    });
  };

  return (
    <div className="student-main-form-container">
      <FormControl>
        <div className="student-name-container">
          <TextField
            id="name"
            label="Name"
            value={student.name}
            onChange={handleStudentNameChange}
            color="secondary"
          />
        </div>
        <div className="student-expenses-container">
          <TextField
            className="student-expenses-container"
            id="expenses"
            label="Expenses"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              maxLength: 13,
              step: 0.01,
              min: 0,
            }}
            onChange={handleStudentExpensesChange}
            value={student.amount === 0 ? "" : student.amount}
          />
        </div>
      </FormControl>
    </div>
  );
};

export default Student;
