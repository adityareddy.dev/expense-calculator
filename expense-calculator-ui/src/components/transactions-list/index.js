import React, { useEffect } from "react";
import axios from "axios";
import { RestartAltTwoTone } from "@mui/icons-material";
import { Button, Alert } from "@mui/material";
import "./index.css";

const TransactionsList = () => {
  const [transactions, setTransactions] = React.useState([]);

  //make a call to our server and get fresh transactions list
  const handleRefreshButtonClick = (e) => {
    if (e) e.preventDefault();
    axios
      .get("/transactions")
      .then((res) => setTransactions(res.data.transactions));
  };

  //make a call to our server to get transactions once on component mounting
  useEffect(() => {
    handleRefreshButtonClick(null);
  });

  return (
    <div className="transactions-page-main-container">
      <p className="transactions-page-main-header">
        Shows the last 6 transactions
      </p>
      <div className="transactions-page-calculate-button">
        <Button
          variant="contained"
          size="large"
          endIcon={<RestartAltTwoTone />}
          onClick={handleRefreshButtonClick}
        >
          Refresh Transactions List
        </Button>
        <br />
        <br />
        {transactions.map((transaction, index) => {
          return (
            <Alert key={index} severity="success">
              {transaction.amount < 0 ? transaction.toGive : transaction.name}{" "}
              must pay{" "}
              {transaction.amount < 0 ? transaction.name : transaction.toGive} $
              {Math.abs(transaction.amount)}
            </Alert>
          );
        })}
      </div>
    </div>
  );
};

export default TransactionsList;
