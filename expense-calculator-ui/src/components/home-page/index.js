import React from "react";
import Student from "../student";
import axios from "axios";
import calculateExchange from "../../utils/calculate-exchange-amount";
import { useDispatch, useSelector } from "react-redux";
import { Button, Alert } from "@mui/material";
import { Calculate, Save, RestartAlt } from "@mui/icons-material";
import "./index.css";

const HomePage = () => {
  //to track the calculated amounts and students involved
  const [amountToPay, setAmountToPay] = React.useState([]);

  //to track if user saved data to file
  const [dataSaved, setDataSaved] = React.useState("");

  //to track if there is an empty field alert
  const [emptyFieldAlert, setEmptyFieldAlert] = React.useState(false);

  //get the whole state from the redux store
  const studentsandAmountsInState = useSelector((state) => state);
  const dispatch = useDispatch();

  //call the caculate util and set the result in state if no empty fields
  const handleCalculateButtonClick = (e) => {
    e.preventDefault();
    const nameError = studentsandAmountsInState.some(
      (student) => !student.name.trim(" ")
    );
    if (nameError) {
      setEmptyFieldAlert(true);
      return 0;
    }

    setEmptyFieldAlert(false);
    const amountToPayObject = calculateExchange(studentsandAmountsInState);
    setAmountToPay([...amountToPayObject]);
    setDataSaved("");
  };

  //post the transactions to the server and reset everything
  const handleSaveButtonClick = (e) => {
    e.preventDefault();
    axios.post("/transactions", amountToPay).then((res) => {
      setDataSaved(res.data.msg);
      setAmountToPay([]);
      setEmptyFieldAlert(false);
      dispatch({ type: "resetState" });
    });
  };

  //reset all fields and the redux store
  const handleResetButtonClick = (e) => {
    e.preventDefault();
    setAmountToPay([]);
    setDataSaved("");
    setEmptyFieldAlert(false);
    dispatch({ type: "resetState" });
  };

  return (
    <div className="home-page-main-container">
      <p className="home-page-main-header">Enter details of each student</p>
      {/* Display alert if any name is empty */}
      {emptyFieldAlert && (
        <Alert severity="error">
          Please don't leave any names empty! Empty amounts will be considered 0
        </Alert>
      )}
      <div className="home-page-students-container">
        <Student key={0} id={0} />
        <Student key={1} id={1} />
        <Student key={2} id={2} />
      </div>
      <div className="home-page-calculate-button">
        <Button
          variant="contained"
          size="large"
          endIcon={<Calculate />}
          onClick={handleCalculateButtonClick}
        >
          Calculate
        </Button>
      </div>
      {/* Display alert if data is saved to file */}
      {dataSaved && <Alert severity="success">{dataSaved}</Alert>}
      {/* Display amounts to exchange if saved in state after calculation */}
      {amountToPay.length > 0 && (
        <div>
          {amountToPay.map((student, index) => {
            return (
              <Alert key={index} severity="success">
                {student.amount < 0 ? student.toGive : student.name} must pay{" "}
                {student.amount < 0 ? student.name : student.toGive} $
                {Math.abs(student.amount)}
              </Alert>
            );
          })}
          <p>Do you want to save these results to the database?</p>
          <div className="home-page-save-button">
            <Button
              variant="contained"
              size="large"
              endIcon={<Save />}
              onClick={handleSaveButtonClick}
            >
              Save
            </Button>
          </div>
          <Button
            variant="contained"
            size="large"
            endIcon={<RestartAlt />}
            onClick={handleResetButtonClick}
          >
            Reset
          </Button>
        </div>
      )}
      <br />
      <br />
    </div>
  );
};

export default HomePage;
