import HomePage from "./components/home-page";
import TransactionsList from "./components/transactions-list";
import "./App.css";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <HomePage />
        <TransactionsList />
      </header>
    </div>
  );
};

export default App;
